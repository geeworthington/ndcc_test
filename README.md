# README #

### What to do ###

Copy the folder to the themes directory in Wordpress & activate the theme.

### What is this repository for? ###

A modified Wordpress theme (twentyseventeen) with the added custom widget and the short code functionality.

Custom Contact Widget in functions.php - Starts line 347
Datetext Shortcode in functions.php - starts line 477

### Who do I talk to? ###

George Worthington - geeworthington@gmail.com
https://www.linkedin.com/in/gworthington/